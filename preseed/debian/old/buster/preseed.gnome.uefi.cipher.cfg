# .. vim: foldmarker=[[[,]]]:foldmethod=marker
#
## Howto use [[[
######################################################################

## With a PXE boot:
#label BusterGnomeCrypto
#    menu label Debian Buster ^Gnome + UEFI + Cipher WITH PRESEED
#    kernel installer/debian/buster/amd64/linux
#    IPAPPEND 2
#    APPEND vga=normal initrd=installer/debian/buster/amd64/initrd_firm.gz auto=true interface=auto netcfg/dhcp_timeout=60 netcfg/choose_interface=auto priority=critical preseed/url=tftp://129.20.203.27/installer/debian/buster/amd64/preseed.gnome.uefi.cipher.cfg

################################################################## ]]]
## Localization [[[
######################################################################

## Install Time
### Preseeding only locale sets language, country and locale.
#d-i debian-installer/locale string en_US

## The values can also be preseeded individually for greater flexibility.
d-i   debian-installer/language                   string      en
d-i   debian-installer/country                    string      FR
d-i   debian-installer/locale                     select      en_US.UTF-8
d-i   debian-installer/fallbacklocale             select      en_US.UTF-8
d-i   localechooser/supported-locales             multiselect fr_FR.UTF-8
d-i   debian-installer/keymap                     string      fr-latin9

## Choose language
d-i   localechooser/languagelist                  select      en

## Choose keyboard configuration
d-i   console-tools/archs                         string      skip-config
d-i   console-keymaps-at/keymap                   select      fr-latin9
d-i   keyboard-configuration/xkb-keymap           select      French

################################################################## ]]]
## Network configuration [[[
######################################################################

## Auto-configure networking
d-i   netcfg/use_autoconfig                       boolean     true

# Disable that annoying WEP key dialog.
d-i   netcfg/wireless_wep                         string

## To set a different link detection timeout (default is 3 seconds).
## Values are interpreted as seconds.
d-i   netcfg/link_detection_timeout               string      20
d-i   netcfg/link_wait_timeout                    string      3

## If you have a slow dhcp server and the installer times out waiting for
## it, this might be useful.
d-i   netcfg/dhcp_timeout                         string      60
d-i   netcfg/dhcpv6_timeout                       string      1

## Network Configuration
d-i   netcfg/get_hostname                         string      unassigned-hostname
d-i   netcfg/get_domain                           string      unassigned-domain

## If non-free firmware is needed for the network or other hardware, you can
## configure the installer to always try to load it, without prompting. Or
## change to false to disable asking.
d-i   hw-detect/load_firmware                     boolean     true

################################################################## ]]]
## Remote install (Manual) [[[
######################################################################

## Use the following settings if you wish to make use of the network-console
## component for remote installation over SSH. This only makes sense if you
## intend to perform the remainder of the installation manually.
#d-i anna/choose_modules string network-console
#d-i network-console/authorized_keys_url string http://10.0.0.1/openssh-key
#d-i network-console/password password r00tme
#d-i network-console/password-again password r00tme

################################################################## ]]]
## Mirror settings [[[
######################################################################

## Mirror
d-i   mirror/country                              string      FR
d-i   mirror/http/hostname                        string      deb.debian.org
d-i   mirror/http/directory                       string      /debian/
d-i   mirror/http/proxy                           string
d-i   mirror/http/mirror                          string      deb.debian.org
d-i   mirror/suite                                string

################################################################## ]]]
## Account [[[
######################################################################

## Enable shadow passwords
d-i   passwd/shadow                               boolean     true

## Allow login as root?
d-i   passwd/root-login                           boolean     true

## Root password in clear text
#d-i   passwd/root-password                        password    r00tme
#d-i   passwd/root-password-again                  password    r00tme
## ... or encrypted with {MD5, SHA512, …) hash
d-i   passwd/root-password-crypted                password    $6$ceGTxMxc$gXajYByJna1cfTjaST3TcF0FfrlSAaEcmCiOMq/DBOuD0tlu8VYQosZPgwcFT4bCuODMErU/fgRxZEeu9c10V0

## Create a normal user account now?
d-i   passwd/make-user                            boolean     false

## User name
#d-i   passwd/user-fullname                        string      bob
#d-i   passwd/username                             string      bob
## User password in clear text
#d-i   passwd/user-password                        password    insecure
#d-i   passwd/user-password-again                  password    insecure
## ... or encrypted with MD5 hash
#d-i   passwd/user-password-crypted                password    $1$098f6bcd4621d373cade4e832627b4f6

## Set User UID
#d-i   passwd/user-uid                             string      1000

################################################################## ]]]
## Clock and time zone setup [[[
######################################################################

## Material clock on UTC
d-i   clock-setup/utc                             boolean     true
d-i   time/zone                                   string      Europe/Paris

## Use NTP to set time
d-i   clock-setup/ntp                             boolean     true
d-i   clock-setup/ntp-server                      string      ntp.univ-rennes1.fr

################################################################## ]]]
## Disk Partitioning/Boot loader [[[
######################################################################

## If the system has only one disk the installer will default to using it.
## Otherwise, the device name must be given
d-i   partman-auto/disk                           string      /dev/nvme0n1 /dev/sda /dev/vda

d-i   partman-auto/init_automatically_partition   select      custom

## Specify the method to use
### - regular: use the usual partition types for the architecture
### - lvm: use LVM to partition the disk
### - crypto: use LVM within an encrypted partition
d-i   partman-auto/method                         string      crypto

## It's not ok to have /boot in the LVM
d-i   partman-auto-lvm/no_boot                    boolean     false

## Remove old LVM configuration
d-i   partman-lvm/device_remove_lvm               boolean     true
d-i   partman-lvm/device_remove_lvm_span          boolean     true

## Remove old RAID configuration
d-i   partman-md/device_remove_md                 boolean     true
d-i   partman-md/confirm                          boolean     true

## Confirm to write the LVM partitions
d-i   partman-lvm/confirm                         boolean     true
d-i   partman-lvm/confirm_nooverwrite             boolean     true

## Keep that one set to true so we end up with a UEFI enabled
## system. If set to false, /var/lib/partman/uefi_ignore will be touched
d-i   partman-efi/non_efi_system                  boolean     true

## Enforce usage of GPT - a must have to use EFI!
d-i   partman-basicfilesystems/choose_label       string      gpt
d-i   partman-basicfilesystems/default_label      string      gpt
d-i   partman-partitioning/choose_label           string      gpt
d-i   partman-partitioning/default_label          string      gpt
d-i   partman/choose_label                        string      gpt
d-i   partman/default_label                       string      gpt

## Volume Group/vg
d-i   partman-auto-lvm/new_vg_name                string      crypt

### From buster, need to specify the expected size
d-i   partman-auto-lvm/guided_size                string      max

d-i   partman-auto/choose_recipe                  select      root-encrypted
d-i   partman-auto/expert_recipe                  string      \
      root-encrypted ::                                       \
              538 300000 1075 free                            \
                      $iflabel{ gpt }                         \
                      $reusemethod{ }                         \
                      method{ efi }                           \
                      format{ }                               \
              .                                               \
              200 200000 400 ext3                             \
                      $primary{ } $bootable{ }                \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext3 }    \
                      mountpoint{ /boot }                     \
              .                                               \
              1000 8000 3000 ext4                             \
                      $lvmok{ }                               \
                      $primary{ }                             \
                      lv_name{ lv_root }                      \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      label{ root }                           \
                      mountpoint{ / }                         \
              .                                               \
              4000 7000 10000 ext4                            \
                      $lvmok{ }                               \
                      lv_name{ lv_usr }                       \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      options/nodev{ nodev }                  \
                      label{ usr }                            \
                      mountpoint{ /usr }                      \
              .                                               \
              4000 6000 8000 ext4                             \
                      $lvmok{ }                               \
                      lv_name{ lv_var }                       \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      options/nodev{ nodev }                  \
                      label{ var }                            \
                      mountpoint{ /var }                      \
              .                                               \
              1000 5000 2000 ext4                             \
                      $lvmok{ }                               \
                      lv_name{ lv_tmp }                       \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      options/nodev{ nodev }                  \
                      options/nosuid{ nosuid }                \
                      label{ tmp }                            \
                      mountpoint{ /tmp }                      \
              .                                               \
              2000 4000 10000 ext4                            \
                      $lvmok{ }                               \
                      lv_name{ lv_home }                      \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      options/nodev{ nodev }                  \
                      label{ home }                           \
                      mountpoint{ /home }                     \
              .                                               \
              512 3000 4000 ext4                              \
                      $lvmok{ }                               \
                      lv_name{ lv_opt }                       \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext4 }    \
                      label{ opt }                            \
                      mountpoint{ /opt }                      \
              .                                               \
              512 1000 100% linux-swap                        \
                      $lvmok{ }                               \
                      lv_name{ lv_swap }                      \
                      method{ swap } format{ }                \
              .                                               \
              100 100 -1 ext3                                 \
                      $lvmok{ }                               \
                      lv_name{ lv_free }                      \
                      method{ format } format{ }              \
                      use_filesystem{ } filesystem{ ext3 }    \
                      label{ free }                           \
                      mountpoint{ /mnt/free }                 \
              .
## Need to put all free space in a temp logical volume/partition
## Otherwise it's the last partition which get all free space

## Allow to not set a swap partition
#d-i partman-basicfilesystems/no_swap              boolean false

## Return to menu if no mount point is assigned to a filesystem
d-i   partman-basicfilesystems/no_mount_point     boolean     false

## Automatically partition without confirmation
d-i   partman/confirm_write_new_label             boolean     true
d-i   partman/choose_partition                    select      finish
d-i   partman/confirm                             boolean     true
d-i   partman/confirm_nooverwrite                 boolean     true

################################################################## ]]]
## Kernel [[[
######################################################################

## Kernel to install
d-i   base-installer/kernel/image                 select      linux-image-amd64

## Drivers to include in the initrd
d-i   base-installer/initramfs-tools/driver-policy select      most

################################################################## ]]]
## APT configuration // Package selection [[[
######################################################################

## Post install APT setup
d-i   apt-setup/uri_type                          select      d-i
d-i   apt-setup/hostname                          string      deb.debian.org
d-i   apt-setup/directory                         string      /debian/
d-i   apt-setup/another                           boolean     false
d-i   apt-setup/security-updates                  boolean     false
d-i   apt-setup/services-select                   multiselect security
d-i   apt-setup/non-free                          boolean     true
d-i   apt-setup/contrib                           boolean     true
d-i   apt-setup/enable-source-repositories        boolean     true

## Ensure to use a network mirror
### Set to false for an offline installation
d-i   apt-setup/use_mirror                        boolean     true

## Choose packages
tasksel tasksel/first                             multiselect ssh-server standard gnome-desktop

## Individual additional packages to install
### tftp-hpa to download an archive (see preseed/late_command at the end of this file)
d-i   pkgsel/include                              string      tftp-hpa

## Whether to upgrade packages after debootstrap.
### Allowed values: none, safe-upgrade, full-upgrade
d-i   pkgsel/upgrade                              select      full-upgrade

## Participate to Popularity Contest (disable for an unconnected server)
d-i   popularity-contest/participate              boolean     true

## For CD/DVD case
### Don't scan for additional CD/DVD
d-i  apt-setup/cdrom/set-first                    boolean     false
d-i  apt-setup/cdrom/set-double                   boolean     false
d-i  apt-setup/cdrom/set-next                     boolean     false
d-i  apt-setup/cdrom/set-failed                   boolean     false

################################################################## ]]]
## Boot loader [[[
######################################################################

## Grub is the default boot loader (for x86). If you want lilo installed
## instead, uncomment this:
#d-i grub-installer/skip boolean true
## To also skip installing lilo, and install no bootloader, uncomment this too:
#d-i lilo-installer/skip boolean true

## This is fairly safe to set, it makes grub install automatically to the MBR
## if no other operating system is detected on the machine.
d-i   grub-installer/only_debian                  boolean     true

## This one makes grub-installer install to the MBR if it also finds some other
## OS, which is less safe as it might not be able to boot that other OS.
d-i   grub-installer/with_other_os                boolean     true

## Since Debian Jessie, the d-i will ask to specify the device for Grub.
## Should use 'default', to use the default system disk
###   Fix: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=759737
d-i   grub-installer/bootdev                      string      default

## Don't force GRUB installation to the EFI removable media path
d-i   grub-installer/force-efi-extra-removable    boolean     false

################################################################## ]]]
## Finishing up the installation [[[
######################################################################

## Avoid that last message about the install being complete.
d-i   finish-install/reboot_in_progress           note
d-i   prebaseconfig/reboot_in_progress            note

## This will prevent the installer from ejecting the CD during the reboot,
## which is useful in some situations.
d-i   cdrom-detect/eject                          boolean     false

## This is how to make the installer shutdown when finished, but not
## reboot into the installed system.
#d-i debian-installer/exit/halt boolean true

## This will power off the machine instead of just halting it.
#d-i debian-installer/exit/poweroff boolean true

## Execute some last commands
#in-target wget -O /tmp/latecommand.tar.gz "https://wiki.101010.fr/lib/exe/fetch.php?media=documentation:administration_systeme:latecommand.tar.gz" --no-check-certificate ; \
d-i preseed/late_command string in-target umount /dev/crypt/lv_free ; \
lvremove -f /dev/crypt/lv_free ; \
in-target /bin/rmdir /mnt/free ; in-target /bin/sed -i '/lv_free/d' /etc/fstab ; \
in-target /bin/sed -i 's/\(^\|^\#\)\(PermitRootLogin\).*/\2 yes/g' /etc/ssh/sshd_config ; \
in-target /usr/bin/tftp 129.20.203.27 -c get scripts/latecommand.tar.gz /tmp/latecommand.tar.gz ; \
in-target tar xzf /tmp/latecommand.tar.gz -C /tmp/ ; \
in-target /bin/sh /tmp/latecommand/post.buster.sh
################################################################## ]]]
