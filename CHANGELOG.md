## Release 1.0.0

### Removed
* No longer download Wheezy release.
* gpxelinux cause it is no longer available in Debian package.
* Debian Jessie entries.

### Bullseye support
* Download new Debian Stable − Bullseye.
* Set new latecommand script (update packages,…).
* Preseed files :
  * Default preseed file.
  * Add Cinnamon preseed (UEFI + LUKS).
  * Add Gnome3 preseeds (UEFI + LUKS or UEFI).
  * Add compute node preseed.

### Improvement
* Add an UEFI entry to manage specific partitions (see #16).
* task-ssh-server is removed (see #18).
* task-french and task-english are removed (see #18).
* Ensure to have minimal documentation available on the system.
* Do not use wireless interface for installation (see #19).
* Gnome preseed is now only available with UEFI (see #20).
* Update PXE files to last version from Bullseye (6.04~git20190206.bf6db5b4).
* Use deb.debian.org fastly instance instead of ftp.fr.debian.org URL.

### Fix
* Remove tasksel only if no X session was installed (see #17).
* Update doc about Debian distribution name and remove useless entries (oldStable).
* No longer define zsh as default shell for root.
* Installation of aptitude, puppet, tmux and zsh move to latecommand script (post.sh).
* Upgrade iwlwifi firmware to version 20210315.

## Release 0.4.1
### Improvement
* Standard task is no longer install for Debian Stretch server with preseed (see #14).
* Ensure to install `python-apt`|Ansible dependencie (see #14).

### Fix
* Ensure to create olddir (/var/log/messages.d) for 'messages' log.
* Remove the "\" for Rsyslog 'messages' definition.

## Release 0.4.0
### Features
* Delete the extra partition for free space with the Debian's preseed (#2).
* Add some backgrounds (#4).
* Update Clonezilla and Gparted to their latest version.
* Swap is now between 25% of the RAM and 32GB at max.
* Add a nvme0n1 disk to allow auto-partition from the preseed file for new SSD connection.
* Add an entry to install Debian Stable with Mate environnment for i386 computers.
* New logrotate configuration for Debian Stretch (#12).

### Fixes
* Use the new IP address.
* Use shellcheck to ensure POSIX compatibility.
* Add information about update netboot installer after each point release (#5).

## Release 0.3.0
### Summary

Features release.

#### Features
* Script to download some diagnostic tools (memtest86+, GParted live) and update the config file.
* Script to download Clonezilla and update the config file.
* Update the PXE files to the latest Jessie version.
* Documentation updates.

#### Fixes
* Debian's preseed: /opt max size is 4Gb for all Debian.
* Fix differences between all preseed files.
* Fix typo in scripts.
* Fix typo in README.

## Release 0.2.2
### Summary

Bugfixe release.

#### Bugfixes
* Correct latecommand archive name.
* Preseed: change the latecommand URL.
* Correct the syslog configuration file path.

## Release 0.2.1
### Summary

Bugfixe release.

#### Bugfixes
* Get the preseed files with GUI from the previous PXE server.
* Ensure to create all directories for `make_debian_initrd_with_firmware.sh` script.
* Correct the path of the preseed files into Debian's menu.cfg.

---

## Release 0.2.0
### Summary

The code move to Gogs.

#### Features
* README.md get many updates.
* scripts/README.md desribe the scripts.
* Move Debian Jessie in front of Wheezy.
* New preseed files (for Jessie/Wheezy, GUI for minions,…).
* New firmwares for the initrd : **myricom** and **qlogic**.
* Add a vda disk to allow auto-partition from the preseed file for VM.

#### Bugfixes
* Grub install is now completely automated.
* Correct the minimum partitions size to correct at least for a 32Gb hdd.
