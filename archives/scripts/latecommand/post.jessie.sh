#!/bin/sh

RSYSLOGD_CONF_SRC="$(dirname $0)/jessie/etc/rsyslog.conf"
RSYSLOGD_CONF_PATH="/etc/rsyslog.conf"
RSYSLOGD_INCLUDE_SRC="$(dirname $0)/jessie/etc/rsyslog.d/"
RSYSLOGD_INCLUDE_PATH="/etc/rsyslog.d/"

LOGROTATE_CONF_SRC="$(dirname $0)/jessie/etc/logrotate.conf"
LOGROTATE_CONF_PATH="/etc/logrotate.conf"
LOGROTATE_INCLUDE_SRC="$(dirname $0)/jessie/etc/logrotate.d/"
LOGROTATE_INCLUDE_PATH="/etc/logrotate.d/"

## Packages {{{
# Ensure to have some basic packages
apt -y install aptitude puppet tmux zsh

# Remove NFS and rpcbind
aptitude -y remove nfs-common rpcbind

### Tasksel {{{

if [ "$(dpkg -l task-french)" ]; then
	# Move default wordlist to american before remove all packages
	select-default-wordlist --set-default=american
	aptitude -y remove tasksel tasksel-data task-english task-french task-ssh-server laptop-detect

	# Reinstall useful french doc and move back to french dict
	aptitude -y install aspell-fr doc-debian-fr doc-linux-fr-text ifrench-gut manpages-fr manpages-fr-extra wfrench
	select-default-wordlist --set-default=".*(F|f)rench.*"

else
	aptitude -y remove tasksel tasksel-data task-english task-ssh-server laptop-detect
fi

### }}}

# Ensure to reinstall openssh-server
aptitude -y install openssh-server openssh-sftp-server

## }}}

### Rsyslog {{{

# Install new Rsyslog configuration
if [ -f "${RSYSLOGD_CONF_PATH}" ]; then
  cp -- "${RSYSLOGD_CONF_PATH}" "${RSYSLOGD_CONF_PATH}".orig
  cp -- "${RSYSLOGD_CONF_SRC}" "${RSYSLOGD_CONF_PATH}"
fi
cp -- "${RSYSLOGD_INCLUDE_SRC}"* "${RSYSLOGD_INCLUDE_PATH}"

# Remove old log files
## Kernel log files
rm -f  /var/log/kern.log
## Authentication log files
rm -f /var/log/user.log /var/log/auth.log
## Mail log files
rm -f /var/log/mail.err /var/log/mail.info /var/log/mail.log /var/log/mail.warn
## System log files
rm -f /var/log/daemon.log /var/log/syslog /var/log/messages

# Restart Rsyslog service
/etc/init.d/rsyslog restart

### }}}

### Logrotate {{{

# Install new Logrotate configuration
if [ -f "${LOGROTATE_CONF_PATH}" ]; then
  cp -- "${LOGROTATE_CONF_PATH}" "${LOGROTATE_CONF_PATH}".orig
  cp -- "${LOGROTATE_CONF_SRC}" "${LOGROTATE_CONF_PATH}"
fi
cp -- "${LOGROTATE_INCLUDE_SRC}"* "${LOGROTATE_INCLUDE_PATH}"

# Create an archive directory for some log files (aptitude, dpkg,…)
mkdir -p -- /var/log/old_logs

# Create the log directory for journald (Systemd), need the configuration Storage=(auto|persistent)
mkdir -p -- /var/log/journal
### }}}


exit 0
