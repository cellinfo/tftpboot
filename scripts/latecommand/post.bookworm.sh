#!/bin/sh

APT_CONF_INCLUDE_SRC="$(dirname $0)/bookworm/etc/apt/apt.conf.d/"
APT_CONF_INCLUDE_PATH="/etc/apt/apt.conf.d/"

APT_SOURCES_SRC="$(dirname $0)/bookworm/etc/apt/sources.list"
APT_SOURCES_PATH="/etc/apt/sources.list"

RSYSLOGD_CONF_SRC="$(dirname $0)/bookworm/etc/rsyslog.conf"
RSYSLOGD_CONF_PATH="/etc/rsyslog.conf"
RSYSLOGD_INCLUDE_SRC="$(dirname $0)/bookworm/etc/rsyslog.d/"
RSYSLOGD_INCLUDE_PATH="/etc/rsyslog.d/"

LOGROTATE_CONF_SRC="$(dirname $0)/bookworm/etc/logrotate.conf"
LOGROTATE_CONF_PATH="/etc/logrotate.conf"
LOGROTATE_INCLUDE_SRC="$(dirname $0)/bookworm/etc/logrotate.d/"
LOGROTATE_INCLUDE_PATH="/etc/logrotate.d/"

# apt configuration {{{

# Ensure to have some default configuration for Apt
cp -- "${APT_CONF_INCLUDE_SRC}"* "${APT_CONF_INCLUDE_PATH}"

# Ensure to have a correct sources.list file for Apt
cp -- "${APT_SOURCES_SRC}" "${APT_SOURCES_PATH}"

# Update repositories and packages
apt update
apt --assume-yes full-upgrade

# Ensure to have aptitude !
apt --assume-yes install -- aptitude

# }}}

# Rsyslog {{{

# Ensure to install Rsyslog daemon
aptitude --assume-yes install -- rsyslog

# Install new Rsyslog configuration
if [ -f "${RSYSLOGD_CONF_PATH}" ]; then
  cp -- "${RSYSLOGD_CONF_PATH}" "${RSYSLOGD_CONF_PATH}".orig
  cp -- "${RSYSLOGD_CONF_SRC}" "${RSYSLOGD_CONF_PATH}"
fi
cp -- "${RSYSLOGD_INCLUDE_SRC}"* "${RSYSLOGD_INCLUDE_PATH}"

# Restart Rsyslog service
systemctl restart rsyslog

# }}}

# Packages {{{

# Ensure to have some basic packages
aptitude --assume-yes install -- gpg tmux vim-nox zsh

# Ensure to remove some "too"-basic packages
aptitude --assume-yes remove -- vim-tiny gnome-initial-setup

# If no X display is expected
if [ ! "$(dpkg --list -- xorg)" ]; then
	## Remove unwanted x11 libs and packages
	aptitude --assume-yes remove -- libgl1 libglx-mesa0 libglx0 libice6 libsm6    \
  libx11-6 libx11-data libx11-xcb1 libxau6 libxaw7 libxcb-dri2-0 libxcb-dri3-0  \
  libxcb-glx0 libxcb-present0 libxcb-shape0 libxcb-shm0 libxcb-sync1            \
  libxcb-xfixes0 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxdmcp6       \
  libxext6 libxfixes3 libxft2 libxi6 libxinerama1 libxkbfile1 libxmu6 libxmuu1  \
  libxpm4 libxrandr2 libxrender1 libxt6 libxtst6 libxv1 libxxf86dga1            \
  libxxf86vm1 x11-apps x11-common x11-session-utils x11-utils x11-xkb-utils     \
  x11-xserver-utils xauth xbase-clients xinit

	if [ "$(dpkg --list -- task-english)" ]; then
		## Remove task-english
		aptitude --assume-yes remove task-english iamerican ibritish ienglish-common ispell util-linux-locales wamerican
	fi
# If X display is expected
elif [ "$(dpkg --list -- xorg)" ]; then
	if [ "$(dpkg --list -- task-english)" ]; then
		## Remove task-english
		aptitude --assume-yes remove task-english
	fi
fi

## Documentation {{{
## Remove task-french
if [ "$(dpkg --list -- task-french)" ]; then
	### Move default wordlist to american before remove all packages
	select-default-wordlist --set-default=american
	aptitude --assume-yes remove -- task-french

	### Reinstall useful french doc and move back to french dict
	aptitude --assume-yes install -- aspell-fr ifrench-gut manpages-fr wfrench
	select-default-wordlist --set-default=".*(F|f)rench.*"
fi

## Ensure to have minimal documentation
aptitude --assume-yes install -- man-db manpages manpages-fr

## }}}

## SSH {{{
## Remove task-ssh-server
if [ "$(dpkg --list -- task-ssh-server)" ]; then
	aptitude --assume-yes remove -- task-ssh-server ncurses-term
fi

## Ensure to install openssh-server
aptitude --assume-yes install -- openssh-server openssh-sftp-server

## }}}

# Ansible dependencies
aptitude --assume-yes install -- python3-apt

## Tasksel {{{
## If tasksel and tasksel-data are the only task* relative packages
if [ "$(dpkg --list -- | grep --count -- '^ii  task')" -eq "2" ]; then
	aptitude --assume-yes remove -- tasksel tasksel-data
fi

## purge configuration files
aptitude --assume-yes purge -- '~c'

## }}}

# }}}

# Grub {{{

# If EFI directory is present
EFI_PATH="/boot/efi"
if [ -d "${EFI_PATH}" ]; then
	## Install grub-efi
	aptitude --assume-yes install -- grub-efi-amd64
	## Get grub device (keep only some patterns, eg. /dev/sda, /dev/vda, /dev/nvme0n1,…)
	GRUB_DEVICE=$(sed --silent "s;^\(/dev/[a-z]\{3\}\|/dev/nvme[a-z0-9]\{3\}\)\(p[0-9]\|[0-9]\) ${EFI_PATH} .*;\1;p" /etc/mtab)
	grub-install --target=x86_64-efi "${GRUB_DEVICE}" 2>/dev/null
	if [ -d "${EFI_PATH}"/EFI ]; then
		### Copy efi entries to a boot directory
		mkdir -p -- "${EFI_PATH}"/EFI/boot
		find "${EFI_PATH}"/EFI/grub -type f -iname "grubx64.efi" -exec cp {} "${EFI_PATH}"/EFI/boot/bootx64.efi \; -quit 2>/dev/null
		find "${EFI_PATH}"/EFI/debian -type f -iname "grubx64.efi" -exec cp {} "${EFI_PATH}"/EFI/boot/bootx64.efi \; -quit
	fi
fi

# }}}

# Logrotate {{{

# Install new Logrotate configuration
if [ -f "${LOGROTATE_CONF_PATH}" ]; then
  cp -- "${LOGROTATE_CONF_PATH}" "${LOGROTATE_CONF_PATH}".orig
  cp -- "${LOGROTATE_CONF_SRC}" "${LOGROTATE_CONF_PATH}"
fi
cp -- "${LOGROTATE_INCLUDE_SRC}"* "${LOGROTATE_INCLUDE_PATH}"

# Create an archive directory for some log files (aptitude, dpkg,…)
mkdir -p -- /var/log/old_logs.d \
	/var/log/alternatives.d \
	/var/log/aptitude.d \
	/var/log/auth.d \
	/var/log/cron.d \
	/var/log/daemon.d \
	/var/log/dpkg.d \
	/var/log/kern.d \
	/var/log/mail.d \
	/var/log/messages.d \
	/var/log/syslog.d

chmod 0750 /var/log/auth.d /var/log/daemon.d /var/log/kern.d /var/log/messages.d /var/log/syslog.d
chown root:adm /var/log/auth.d /var/log/daemon.d /var/log/kern.d /var/log/messages.d /var/log/syslog.d

# Create the log directory for journald (Systemd), need the configuration Storage=(auto|persistent)
mkdir -p -- /var/log/journal
# }}}

exit 0
