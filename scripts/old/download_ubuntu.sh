#!/bin/bash

# This script do the following:
#	 Download last Ubuntu LTS 16.04 (xenial)
#	 Download Ubuntu 17.10 (artful)
#	 Make a symlink to access to the last LTS

if command -v in.tftpd > /dev/null; then
	source /etc/default/tftpd-hpa
else
	TFTP_DIRECTORY="/srv/tftp"
fi

# last Long Term Support distribution name
LTS="focal"

for DISTRO in artful ${LTS}; do
	for ARCH in amd64 i386; do
		mkdir -p ${TFTP_DIRECTORY}/installer/ubuntu/${DISTRO}/${ARCH}
		pushd ${TFTP_DIRECTORY}/installer/ubuntu/${DISTRO}/${ARCH} > /dev/null
		#wget http://fr.archive.ubuntu.com/ubuntu/dists/${DISTRO}/main/installer-${ARCH}/current/images/netboot/ubuntu-installer/${ARCH}/linux -O linux
		#wget http://fr.archive.ubuntu.com/ubuntu/dists/${DISTRO}/main/installer-${ARCH}/current/images/netboot/ubuntu-installer/${ARCH}/initrd.gz -O initrd.gz
		wget --quiet http://fr.archive.ubuntu.com/ubuntu/dists/${DISTRO}/main/installer-${ARCH}/current/images/netboot/ubuntu-installer/${ARCH}/linux -O linux
		wget --quiet http://fr.archive.ubuntu.com/ubuntu/dists/${DISTRO}/main/installer-${ARCH}/current/images/netboot/ubuntu-installer/${ARCH}/initrd.gz -O initrd.gz
		popd > /dev/null
	done
done

# Link the LTS distribution name to lts
if [ -L "${TFTP_DIRECTORY}/installer/ubuntu/lts" ]; then
	unlink ${TFTP_DIRECTORY}/installer/ubuntu/lts
fi
ln -s ${LTS} ${TFTP_DIRECTORY}/installer/ubuntu/lts

exit 0
