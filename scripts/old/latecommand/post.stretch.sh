#!/bin/sh

APT_CONF_INCLUDE_SRC="$(dirname $0)/stretch/etc/apt/apt.conf.d/"
APT_CONF_INCLUDE_PATH="/etc/apt/apt.conf.d/"

RSYSLOGD_CONF_SRC="$(dirname $0)/stretch/etc/rsyslog.conf"
RSYSLOGD_CONF_PATH="/etc/rsyslog.conf"
RSYSLOGD_INCLUDE_SRC="$(dirname $0)/stretch/etc/rsyslog.d/"
RSYSLOGD_INCLUDE_PATH="/etc/rsyslog.d/"

LOGROTATE_CONF_SRC="$(dirname $0)/stretch/etc/logrotate.conf"
LOGROTATE_CONF_PATH="/etc/logrotate.conf"
LOGROTATE_INCLUDE_SRC="$(dirname $0)/stretch/etc/logrotate.d/"
LOGROTATE_INCLUDE_PATH="/etc/logrotate.d/"

# apt configuration {{{

# ensure to have some default configuration for Apt
cp -- "${APT_CONF_INCLUDE_SRC}"* "${APT_CONF_INCLUDE_PATH}"

# }}}

## Packages {{{

# update repositories and packages
apt update
apt -y full-upgrade

# Ensure to have some basic packages
apt -y install aptitude tmux vim-nox zsh

# Ensure to remove some "too"-basic packages
aptitude -y remove vim-tiny


# Ensure to have some systemd basic packages
aptitude -y install dbus libpam-systemd

# Remove NFS and rpcbind
aptitude -y remove nfs-common rpcbind

### Documentation {{{
# If no X display is expected
if [ ! "$(dpkg -l xorg)" ]; then
	## Remove unwanted x11 lib
	aptitude -y remove libx11-6 libx11-data libxau6 libxcb1 libxdmcp6 libxext6 libxmuu1 xauth
	if [ "$(dpkg -l task-english)" ]; then
		## Remove task-english
		aptitude -y remove task-english iamerican ibritish ienglish-common ispell util-linux-locales wamerican
	fi
else
	if [ "$(dpkg -l task-english)" ]; then
		## Remove task-english
		aptitude -y remove task-english
	fi
fi

# Ensure to have minimal documentation
aptitude -y install man-db manpages

### }}}

### SSH {{{
# Remove task-ssh-server
if [ "$(dpkg -l task-ssh-server)" ]; then
	aptitude -y remove task-ssh-server
fi

# Ensure to install openssh-server
aptitude -y install openssh-server openssh-sftp-server

### }}}

# Ansible dependencies
aptitude -y install python-apt

### Tasksel {{{
# If tasksel and tasksel-data are the only task* relative packages
if [ "$(dpkg -l | grep -c task)" -eq "2" ]; then
	aptitude -y remove tasksel tasksel-data
fi

# purge configuration files
aptitude -y purge '~c'

### }}}

## }}}

# Grub {{{

## If EFI directory is present
EFI_PATH="/boot/efi"
if [ -d "${EFI_PATH}" ]; then
	## Install grub-efi
	aptitude install -y grub-efi-amd64
	## Get grub device (keep only some patterns, eg. /dev/sda, /dev/vda, /dev/nvme0n1,…)
	GRUB_DEVICE=$(sed -n "s;^\(/dev/[a-z]\{3\}\|/dev/nvme[a-z0-9]\{3\}\)\(p[0-9]\|[0-9]\) ${EFI_PATH} .*;\1;p" /etc/mtab)
	grub-install --target=x86_64-efi "${GRUB_DEVICE}" 2>/dev/null
	if [ -d "${EFI_PATH}"/EFI ]; then
		## Copy efi entries to a boot directory
		mkdir -p -- "${EFI_PATH}"/EFI/boot
		find "${EFI_PATH}"/EFI/grub -type f -iname "grubx64.efi" -exec cp {} "${EFI_PATH}"/EFI/boot/bootx64.efi \; -quit 2>/dev/null
		find "${EFI_PATH}"/EFI/debian -type f -iname "grubx64.efi" -exec cp {} "${EFI_PATH}"/EFI/boot/bootx64.efi \; -quit
	fi
fi

### }}}

### Rsyslog {{{

# Install new Rsyslog configuration
if [ -f "${RSYSLOGD_CONF_PATH}" ]; then
  cp "${RSYSLOGD_CONF_SRC}" "${RSYSLOGD_CONF_PATH}"
fi
cp -- "${RSYSLOGD_INCLUDE_SRC}"* "${RSYSLOGD_INCLUDE_PATH}"

# Restart Rsyslog service
systemctl restart rsyslog

### }}}
### Logrotate {{{

# Install new Logrotate configuration
if [ -f "${LOGROTATE_CONF_PATH}" ]; then
  cp "${LOGROTATE_CONF_SRC}" "${LOGROTATE_CONF_PATH}"
fi
cp -- "${LOGROTATE_INCLUDE_SRC}"* "${LOGROTATE_INCLUDE_PATH}"

# Create an archive directory for some log files (aptitude, dpkg,…)
mkdir -p -- /var/log/old_logs.d /var/log/aptitude.d /var/log/dpkg.d /var/log/alternatives.d /var/log/syslog.d /var/log/cron.d /var/log/daemon.d /var/log/kern.d /var/log/lpr.d /var/log/mail.d /var/log/auth.d /var/log/messages.d

chmod 0750 /var/log/auth.d /var/log/daemon.d /var/log/kern.d /var/log/messages.d /var/log/syslog.d
chown root:adm /var/log/auth.d /var/log/daemon.d /var/log/kern.d /var/log/messages.d /var/log/syslog.d

# Create the log directory for journald (Systemd), need the configuration Storage=(auto|persistent)
mkdir -p -- /var/log/journal
### }}}

exit 0
