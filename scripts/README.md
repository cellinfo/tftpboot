# Scripts

1. [Description](#description)
2. [Scripts list](#scripts-list)
    * [Download Debian](#download_debiansh)
    * [Download Ubuntu](#download_ubuntush)
    * [Make Debian Initrd with Firmware](#make_debian_initrd_with_firmwaresh)
    * [Debian late_command](#debian-late_command)
    * [Download diag tools](#download_diag_toolssh)
    * [Download Clonezilla](#download_clonezillash)
3. [Update](#update)

## Description
Set of scripts to download and generate necessary files to allow differents GNU/Linux distributions to boot through the network.

## Scripts list

### download_debian.sh
The script will provide netboot installers for all Debian (>=oldStable) for both **i386** and **amd64** :
  * Store the files to **/srv/tftp** (default).
  * Download Debian Linux kernel and initrd files for all Debian distributions (Stable : **Bullseye**; oldStable : **Buster**; oldoldStable : **Stretch**; Unstable : **Sid**) and for **i386** and **amd64** architectures.
  * Offer an example menu file to use with a TFTP server (default_path/installer/debian/menu.cfg.example).

### download_ubuntu.sh
The script will provide netboot installers for the latest LTS and latest advanced Ubuntu distribution for both **i386** and **amd64** :
  * Store the files to **/srv/tftp** (default).
  * Download Ubuntu Linux kernel and initrd files for the two lasts Ubuntu distributions (LTS-16.04 : **Xenial**; 15.10 : **Wily**) and for **i386** and **amd64** architectures.
  * Make a symlink to access to the LTS (default_path/installer/ubuntu/lts) instead of it's codename.

### make_debian_initrd_with_firmware.sh
The script will provide Debian's netboot installers with **additionnals firmwares** for all Debian (>=oldStable) for **amd64** only :
  * Store the files to **/srv/tftp** (default).
  * Store temp files to **/tmp/tmp.XXXXXXXXXX** (with `mktemp`).
  * Download Debian's initrd files for all Debian distributions (Stable : **Bullseye**; oldStable : **Buster**; oldoldStable : **Stretch**; Unstable : **Sid**) for **amd64** architecture only.
  * Download additionnals firmwares :
    * **bnx2** : For Broadcom NetXtremeII network adapters.
    * **linux-nonfree** : For Tigon network adapters.
    * **myricom** : For Myricom Myri-10G Ethernet adapters (IBM server).
    * **qlogic** : For QLogic Infiniband, SCSI, Fibre Channel/FCoE adapters.
    * **iwlwifi** : Binary firmware for Intel Wireless cards
  * Extract initrd and firmwares packages to build a new initrd.

### Debian late_command

#### Description
Some useful configurations after a Debian installation :
  * Smarter log management (mainly for rotation and old logs).
    * Move `rsyslog` default configuration to two files (__/etc/rsyslog.conf__ and __/etc/rsyslog.d/99syslog.conf__).
    * `logrotate` now rotate logs to specific directories (such as __/var/log/syslog.d__, __/var/log/mail/__,…) instead of __/var/log/__.
    * Create these new directories.
  * Packages :
    * Add minimal packages for a working system (aptitude, tmux and zsh).
    * Remove some useless packages (nfs rpcbind,…).
    * Ensure an openssh-server is available.
  * Permit root SSH login with password (no standard user…).
  * …

Specific to Debian Bullseye :
  * Add python3-apt (Ansible dependency).

Specific to Debian Stretch and Buster :
  * Add python-apt (Ansible dependency).

#### How-to use it
  * First, make an archive of the script and directories :
```
cd scripts/ ;
tar czvf latecommand.tar.gz latecommand/
```
  * Then, it can be use at the end of a Debian preseed file :
```
d-i preseed/late_command string in-target /usr/bin/tftp ${IP.SRV.TFTP} -c get ${PATH/TO/TFTPD/ROOT}/scripts/latecommand.tar.gz /tmp/latecommand.tar.gz ; \
in-target tar xzf /tmp/latecommand.tar.gz -C /tmp/ ; \
in-target /bin/sh /tmp/latecommand/post.sh
```

### download_diag_tools.sh
- The script will download some diagnostic tools :
  * memtest86 from [memtest86's official website][memtest official website].
  * GParted Live from the [sourceforge's repository][gparted sourceforge] for both amd64 and i686. See the [official documentation][gparted pxe] for more informations.
- Provide an example of configuration file.

### download_clonezilla.sh
* Download PXE files for Clonezilla live from [OSDN][OSDN url]. A Sourceforge repository is also available but…; see the [Clonezilla download page][clonezilla download] for more informations.
* Download for both **amd64** and **i686**.
* Make a example configuration file.
* More informations on the [Clonezilla website][clonezilla via pxe server].

## Update
The netboot's installers, at least for Debian, need to be update **after each point realease**. Just think about it if you got any issue during any installation from PXE.
* Debian : `sudo ./scripts/download_debian.sh`
* Debian's initrd with additionnals firmwares : `sudo ./scripts/make_debian_initrd_with_firmware.sh`
* Ubuntu : `sudo ./scripts/download_ubuntu.sh`

Some tools depends of a static version in their script. So this is the list i check to got their new version :
  * [Clonezilla download page][clonezilla download]
  * [Gparted sourceforge's repository][gparted sourceforge]
  * [Memtest86's official website][memtest official website] (quite useless, last update was ~3 years ago ^^)

[memtest official website]: http://www.memtest.org/#downiso "Memtest86+ download"
[OSDN url]: https://osdn.jp/projects/clonezilla/
[gparted sourceforge]: https://sourceforge.net/projects/gparted/files/gparted-live-stable/ "Gparted on sourceforge"
[gparted pxe]: http://gparted.org/livepxe.php "GParted Live on PXE Server"
[clonezilla download]: http://clonezilla.org/downloads/download.php?branch=stable
[clonezilla via pxe server]: http://clonezilla.org/livepxe.php "Clonezilla Live on PXE server"
