# tftpboot

1. [Overview](#overview)
2. [Description](#description)
    * [PXE files](#pxe-files)
    * [Configuration files](#configuration-files)
    * [Preseed](#preseed)
    * [Scripts](#scripts)
3. [How to use it](#how-to-use-it)
4. [TODO](#todo)

## Overview

All needed files to set up a PXE server for both standard bios and UEFI (32b and 64b).

## Description

### PXE files
Use some bootloaders files provide by :
  * `pxelinux` debian package :
    * gpxelinux.0
    * pxelinux.0
  * `syslinux-efi` debian package :
    * syslinux.efi
  * `syslinux-common` debian package :
    * chain.c32
    * ldlinux.c32
    * ldlinux.e32
    * ldlinux.e64
    * libcom32.c32
    * libutil.c32
    * menu.c32
    * reboot.c32
    * vesamenu.c32

* To get those files from the packages of your repositories :

``` sh
mkdir -p temp_pxe_files
cd temp_pxe_files
mkdir bios efi32 efi64
aptitude download pxelinux syslinux-common syslinux-efi
ar p pxelinux*.deb data.tar.xz | tar xJ -C . --strip-components=4 ./usr/lib/PXELINUX/pxelinux.0
ar p pxelinux*.deb data.tar.xz | tar xJ -C bios/ --strip-components=4 ./usr/lib/PXELINUX/pxelinux.0
ar p syslinux-efi*.deb data.tar.xz | tar xJ -C efi32/ --strip-components=5 ./usr/lib/SYSLINUX.EFI/efi32/syslinux.efi
ar p syslinux-efi*.deb data.tar.xz | tar xJ -C efi64/ --strip-components=5 ./usr/lib/SYSLINUX.EFI/efi64/syslinux.efi
ar p syslinux-common*.deb data.tar.xz | tar xJ -C . --strip-components=6 ./usr/lib/syslinux/modules/bios/{chain,ldlinux,libcom32,libutil,menu,reboot,vesamenu}.c32
ar p syslinux-common*.deb data.tar.xz | tar xJ -C bios/ --strip-components=6 ./usr/lib/syslinux/modules/bios/{chain,ldlinux,libcom32,libutil,menu,reboot,vesamenu}.c32
ar p syslinux-common*.deb data.tar.xz | tar xJ -C efi32/ --strip-components=6 ./usr/lib/syslinux/modules/efi32/{chain.c32,ldlinux.e32,libcom32.c32,libutil.c32,menu.c32,reboot.c32,vesamenu.c32}
ar p syslinux-common*.deb data.tar.xz | tar xJ -C efi64/ --strip-components=6 ./usr/lib/syslinux/modules/efi64/{chain.c32,ldlinux.e64,libcom32.c32,libutil.c32,menu.c32,reboot.c32,vesamenu.c32}
```

  * The MD5 checksum can mismatch between the files from this repo and those from Debian's packages.
* Then create symlinks to avoid to duplicate files :

``` sh
for DIR in bios efi32 efi64; do
ln -s ../config "${DIR}"
ln -s ../installer "${DIR}"
ln -s ../preseed "${DIR}"
ln -s ../pxelinux.cfg "${DIR}"
done
```


### Configuration files
Contains the files which defines the PXE menu. The [first one][main menu.cfg] will :
  * Call all others menu.cfg from subdirectories (debian/, clonezilla/,…) as an entry.
  * Define a timeout (4 seconds).
  * Define the default entry (boot on local first disk).
  * …

#### config/debian/menu.cfg
1. Provide PXE entries for :
  * Debian Bookworm (Stable) amd64 with additional firmwares (see [make_debian_initrd_with_firmware.sh][debian initrd with firmware script] script).
  * Debian Bookworm (Stable) amd64 with additional firmwares and preseed file.
  * Debian Bookworm (Stable) amd64 for compute nodes with preseed.
  * Debian Bullseye (oldStable) amd64 with additional firmwares (see [make_debian_initrd_with_firmware.sh][debian initrd with firmware script] script).
  * Debian Bullseye (oldStable) amd64 with additional firmwares and preseed file.
  * Debian Bullseye (oldStable) amd64 for compute nodes with preseed.
  * Debian Bullseye (oldStable) amd64 + UEFI + LUKS for minions with preseed (Cinnamon as default GUI).
  * Debian Bullseye (oldStable) amd64 + UEFI + LUKS for minions with preseed (Gnome3 as default GUI).
  * Debian Bullseye (oldStable) amd64 + UEFI for minions with preseed (Gnome3 as default GUI).
  * Debian Sid (unstable) amd64.
  * Debian Sid (unstable) amd64 with additional firmwares (see [make_debian_initrd_with_firmware.sh][debian initrd with firmware script] script).
  * Debian Sid (unstable) i386.
2. If you want to use with your own TFTPD server, you need te set your IP server ! Replace all occurrences of *129.20.203.27* by the IP of your TFTPD server to be able to load the preseed files !

#### config/ubuntu/menu.cfg
Provide PXE entries for :
  * Ubuntu LTS (16.04) amd64.
  * Ubuntu LTS (16.04) i386.
  * Latest non-LTS Ubuntu (15.10) amd64.
  * Latest non-LTS Ubuntu (15.10) i386.

### Preseed
A preseed file can be loaded by the Debian-Installer to answer all questions
Preseeding provides a way to set answers to questions asked by the Debian-Installer, without having to manually enter the answers. This makes it possible to fully automate most types of installation and even offers some features not available during normal installations. More informations on [Debian wiki][preseed informations] and in [Debian sources][debian sources] (search for partman-auto, partman-lvm,… packages).

#### Commons configurations
* Localization :
  * Language : English
  * Country : France
  * Locale : en_US.UTF-8 (US English)
  * Keyboard : fr-latin9 (French)
* Hardware :
  * Network configuration (IP, hostname, domain) : automatic (from DHCP server)
  * Load additional firmwares if availables.
* Account :
  * Create a root account.
  * root password : SHA-256 hash of the password
  * Don't create a normal user (be sure to **allow remote connection for root** ! See [late_command info][debian latecommand howto]).
* Timezone
  * Timezone : Europe/Paris
  * NTP server : ntp.univ-rennes1.fr
* Partitionning
  * Use the entire first disk (sda, vda aka virtual hdd or nvme0n1) with LVM.
  * Purge all LVM, RAID,… partitions or informations already set on the first disk.
  * Partitionning :

| Device    | Mount point | Size (Min - Max)       | Description                                                            |
|-----------|-------------|------------------------|------------------------------------------------------------------------|
| /dev/sda1 | /boot       | 200 - 250 Mb           | Bootloader (Grub)                                                      |
| /dev/sda2 | LVM         | 16 Gb - 100%           | Physical Volume for LVM                                                |
| lv_root   | /           | 1 - 2 Gb               | Root partition                                                         |
| lv_usr    | /usr        | 4 - 10 Gb              | Non-essential system resources (binaries, libraries, documentations,…) |
| lv_var    | /var        | 4 - 8 Gb               | Variable files (databases, cache, log,…)                               |
| lv_tmp    | /tmp        | 1 - 2 Gb               | Temporary files cleaned between system reboots                         |
| lv_home   | /home       | 2 - 3 Gb               | User's home directories (saved files, personnal settings,…)            |
| lv_srv    | /srv        | 512 MB - 2 Gb          | Specific datas for this system                                         |
| lv_opt    | /opt        | 512 MB - 4 Gb          | Optional application                                                   |
| lv_swap   | SWAP        | 512 Mb - 100% RAM size | Swap sapce                                                             |
| lv_free   | /mnt/free   | free space             | See below                                                              |
* APT :
  * Installation from internet (repository : deb.debian.org)
  * Select **ssh-server** and **standard** in _tasksel_ for installation.
  * Add additional packages : **tftp-hpa** (to be able to download the [latecommand archive from the PXE server][debian latecommand howto]).
* Bootloader :
  * Install Grub on the MBR of the first disk.
* late_command
  * Permit root login via SSH with it's password.
  * Download and extract a _latecommand_ archive from [this repository][debian latecommand description].
  * Run the script from this archive.

------

**lv_free - /mnt/free**
: The preseed's partitionning need to allocate all the disk space. If it's not define, it will put in the last defined partition all extra free space. So **lv_free** is a temporary LV that you can destroy and use the space to resize any other LV.

#### Specific configurations
* You can choose a desktop environment :
  * Debian Stable (aka Bullseye) : Cinnamon or Gnome3
* Partitionning differences for Debian with a desktop environment :

| Device  | Mount point | Size (Min - Max)  | Description                                                 |
|---------|-------------|-------------------|-------------------------------------------------------------|
|  sda1   | /boot/efi   | 512 - **1024** Mb | EFI partition (from Buster)                                 |
| lv_root | /           |   1 - **3** Gb    | root partition                                              |
| lv_home | /home       |   2 - **10** Gb   | User's home directories (saved files, personnal settings,…) |
| lv_srv  | NOT PRESENT |   NOT PRESENT     |                                                             |

#### Informations
* You can use the command : `mkpasswd -m sha-512` to generate a SHA-512 hash for a password.

#### Known issues

##### Volume group name already in use
Despite some preseed's options, these error might occur if a VG_NAME is :

* present on a "first" hard disk
* expected to be created on another hard disk

To fix this :

* Double check the hard disk that should be use for the operating system.
* Manually remove the "previous" VG : `vgremove VG_NAME` and restart the installer.

### Scripts

See the [scripts's README.md][scripts readme.md] file for more informations.

## How to use it
* Put the data of this repository on the PXE server :
  * Clone the repository with https URL or with SSH to **/var/lib/tftpboot**.
  * Download the last archive : `wget https://git.ipr.univ-rennes.fr/cellinfo/tftpboot/archive/master.tar.gz`
* Install a TFTP server (`tftpd-hpa` package for Debian's based systems or `tftp-server` for Redhat's based systems).
  * Start the tftpd's service :
    * `sudo service tftpd-hpa start`
    * `sudo systemctl start tftpd-hpa`
  * You can test if the TFTP server provide the files with a TFTP client (`tftp-hpa`) by downloading a file with the command : `tftp $IP.SRV.TFTP -c get README.md /tmp/README.md_from_tftpd_server`
* Set your IP in some files instead of the default one *129.20.203.27* :
  * In configurations files to be able to call Debian's Preseed files or download squashfs filesystem (for Clonezilla, GParted,…): `sudo find config/ -iname "*.cfg" -type f -exec sed -i "s/129.20.203.27/111.222.333.444/g" {} \;`
  * In Preseed files, to be able to download the late_command archive from your TFTPD server : `sudo find preseed/ -type f -exec sed -i "s/129.20.203.27/111.222.333.444/g" {} \;`
  * In scripts to generate examples configuration files with the right IP address : `sudo find scripts/ -iname "*.sh" -type f -exec sed -i "s/129.20.203.27/111.222.333.444/g" {} \;`
* Run scripts to download needed PXE files (see the [scripts's README.md][scripts readme.md] for more informations) :
  * Debian : `sudo ./scripts/download_debian.sh`
  * Debian's initrd with additional firmwares : `sudo ./scripts/make_debian_initrd_with_firmware.sh`
  * Ubuntu : `sudo ./scripts/download_ubuntu.sh`
  * Diagnostic tools (Memtest86+, GParted Live,…) : `sudo ./scripts/download_diag_tools.sh`
  * Clonezilla : `sudo ./scripts/download_clonezilla.sh`
  * Generate the late_command archive : `cd scripts/ ; sudo tar czvf latecommand.tar.gz latecommand/`


[main menu.cfg]: ./config/menu.cfg
[debian menu.cfg]: ./config/debian/menu.cfg
[preseed informations]: https://wiki.debian.org/DebianInstaller/Preseed
[debian sources]: https://sources.debian.org/
[scripts readme.md]: ./scripts/README.md "scripts's README.md for tftpboot"
[debian initrd with firmware script]: ./scripts/README.md#make_debian_initrd_with_firmwaresh "informations about the Debian's initrd with additional firmwares"
[debian latecommand description]: ./scripts/README.md#debian-late_command  "informations about the Debian's late_command script"
[debian latecommand howto]: ./scripts/README.md#how-to-use-it "how-to use the Debian's late_command"
[debian latecommand post.sh script]: ./scripts/latecommand/post.sh
